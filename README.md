# PLDT_R051_Unlock

## Requirements

-   PLDT R051 Router with Firmware v008 or v010
-   LAN cable (already included with PLDT R051 package)
-   Windows PC or Linux (Not tested but should be ok)
-   Firmware file and tftpd executable

## Steps

Extract firmware binary and tftp executable to your preferred directory.

Go to Network Adapter settings and set static IP for your LAN port, which as follows:

```
IP: 10.10.10.3
Subnet: 255.255.255.0
Gateway: 10.10.10.123
```

Plug one end of the LAN cable to your PC and the other one on the R051 LAN1 port. Do not plug in the power yet.

Run the tftp64 executable, allow it to the Windows Firewall for public and private connection.

Press and hold the RESET/WPS button, plug in the power to the DC jack while holding the button.

```
tftpd
Connection received from 10.10.10.123 on port 2938 [06/05 18:27:46.451]
Read request for file <firmware.bin>. Mode octet [06/05 18:27:46.452]
OACK: <timeout=3,> [06/05 18:27:46.461]
Using local port 56701 [06/05 18:27:46.461]
<firmware.bin>: sent 15745 blks, 8060928 bytes in 3 s. 0 blk resent [06/05 18:27:49.535]
```

You will notice that the tftp will indicate an upload progress, this means the router is now being flashed. Let go of the reset button and the router will now reboot and will be successfully unlocked/openlined.

Default password: pldthome

Tested only on Globe and Smart SIM, No band locking, YMMV!

## Authors and acknowledgment
-   Firmware from: `https://youtu.be/0nsrSd-0TAc`
-   TFTPD from `https://bitbucket.org/phjounin/tftpd64/downloads/`

## License

CC BY-SA 4.0 for the tutorial. Files under their respective licenses.
